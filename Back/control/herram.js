/**
 * @global
 * @name Herramientas
 * @description Definición de herramientas comunes a toda la API
 * @module
 */

'use strict'
var globales = require('../globales');

var requestJSON = require('request-json');

var baseMLabURL = globales.baseMLabURL;
var apikeyMLab = globales.apikeyMLab;


var meses = globales.meses;
var baseMLabURL = globales.baseMLabURL;
var apikeyMLab = globales.apikeyMLab;
var diasSemana = globales.diasSemana;
var hoy = new Date();
var fecha = hoy.getDate() + "/" + (hoy.getMonth() + 1) + "/" + hoy.getFullYear();
var hora = hoy.getHours() + ":" + hoy.getMinutes() + ":" + hoy.getSeconds() + "." + hoy.getMilliseconds();
var selloTiempo = fecha + "-" + hora;


/** @function
 * @name saludar
 * @description Método que genera un saludo, útil para probar la API.
 * @author IBC
 */
function saludar(req, res) {
  res.send('API desde Node.js, hoy es ' + diasSemana[hoy.getDay()] + ', ' + hoy.getDate() + ' de ' + meses[hoy.getMonth()] + ' de ' + hoy.getFullYear());
};

/** @function
 * @name help
 * @description Método que realiza un redireccionamiento a la página de ayuda.
 * Existe un shell sript "creoJsDoc.sh" que genera la documentación, por parámetro se le indica el directorio donde se publicará.
 * Se debe modificar la constante global helpURL si se cambia la URL de help.
 * @author IBC
 */
function help(req, res) {
  res.redirect(globales.helpURL);
  // res.send('Descarga de Guía de Refencia de la API ver.1.0.0');
};

/** @function
 * @name escribirLog
 * @description Método que graba un log de consola.
 * @author IBC
 */
function escribirLOG(msg) {
  var hoy = new Date();
  var fecha = hoy.getFullYear() + "/" + (hoy.getMonth() + 1) + "/" + hoy.getDate();
  var hora = hoy.getHours() + ":" + hoy.getMinutes() + ":" + hoy.getSeconds() + "." + hoy.getMilliseconds();
  var timeStamp = fecha + "-" + hora;
  var mensa = {
    timeStamp,
    "msg": msg
  };

  console.log(JSON.stringify(mensa));
}



module.exports = {
  saludar,
  escribirLOG,
  help
}
