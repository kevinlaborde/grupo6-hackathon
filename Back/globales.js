/**
 * @global
 * @name Globales
 * @description Definición de valores globales de toda la API
 * @module
 */

/** @constant */
const baseMLabURL = "https://api.mlab.com/api/1/databases/hack2019/collections/";
/** @constant */
const apikeyMLab = "apiKey=SB0IVqp6dnAjQYfIwpa-nEzFYmN2SYr9";
/** @constant */
const meses = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
/** @constant */
const diasSemana = new Array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado");
/** @constant */
const baseApiIA = "https://gateway.watsonplatform.net/assistant/api/v1/workspaces/144560fc-d094-4011-945a-7f164b4c47fb/message?version=2019-02-28";
/** @constant */
const apikeyIA = "Authorization=Basic YXBpa2V5OnBDbHlXa1RHSWM1WVU2cFBGX1pJVWx1YlF0RGd2cm8yMkxycTl5MWd2Z280";
const contType = "Content-Type=application/json";

module.exports = {
  baseMLabURL: baseMLabURL,
  apikeyMLab: apikeyMLab,
  meses: meses,
  diasSemana: diasSemana,
  baseApiIA: baseApiIA,
  apikeyIA: apikeyIA,
  contType: contType
  
}
