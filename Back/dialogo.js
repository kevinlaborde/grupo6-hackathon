/**
 * @name dialogo
 * @version 1.0.0
 * @module
 * @description Grupo de métodos que implementan el diálogo con el usuario
 */




const saltRounds = 10;



var jwt = require('jsonwebtoken');

var requestJSON = require('request-json');
var globales = require('../globales');
var herramientas = require('./herram');

var baseMLabURL = globales.baseMLabURL;
var apikeyMLab = globales.apikeyMLab;
var hoy = new Date();
var hoyFmt = hoy.getDate() + "/" + (hoy.getMonth() + 1) + "/" + hoy.getFullYear();
var baseIAURL = globales.baseApiIA;
var apikeyIA = globales.apikeyIA;
var ContType = globales.contType;


/** @method
 * @name dialogar
 * @summary Método para manejar el diálogo con  los usuarios
 * @description Recibe el texto ingresado por el cliente, se comunica con IA, y de acuerdo a la respuesta consulta una base para saber si debe agregar algo a la respuesta
 * @author Grupo 6
 * @async
 */
function dialogar(req, res) {
  herramientas.escribirLOG("-> " + req.method + " " + req.url + " <BEGIN>");

  herramientas.escribirLOG("-> " + JSON.stringify(req.body));
  var msgDB = "";
  var httpClient = requestJSON.createClient(baseIAURL);
  httpClient.headers['Authorization'] = "Basic YXBpa2V5OnBDbHlXa1RHSWM1WVU2cFBGX1pJVWx1YlF0RGd2cm8yMkxycTl5MWd2Z280";
  httpClient.headers['Content-Type'] = "application/json";
  
  httpClient.post("", req.body,
    function(err, respuestaIA, body) {
      var response = {};
      var respuesta = JSON.stringify(body);
      var respJson = JSON.parse(respuesta);
      if (err) {
        response = {
          "msg": "Error obteniendo respuesta."
        }
        res.status(500);
      } else {
          if (respJson.intents.length > 0) {
            var textoResp = respJson.intents[0].intent;    
          } else {
            var textoResp = "";    
          }
          var codMsg = 0;
        
        /*Logica de mesaje adicional a consultar en la BD*/
        switch(textoResp) {
          case "hello":
            codMsg = 1;
            break;
          case "prestamo":
            codMsg = 3;
            break;
          case "cuenta":
            codMsg = 2;
            break;
          case "tarjeta":
              codMsg = 4;
              break;
          case "bye":
            codMsg = 5;
            break;
          default:
            codMsg = 1;
            
        }
        herramientas.escribirLOG("--> Antes de cons DB:"  + codMsg + " " + textoResp);

        /*Consulta de la BD de acuerdo al código del mensaje       */
        
        var httpClient2 = requestJSON.createClient(baseMLabURL);
        var queryString = 'q={"id":' + codMsg + '}&';
        httpClient2.get('mensajes?' + queryString + apikeyMLab,
          function(err2, respuestaMLab2, body2) {
            
            if (err2) {
              msgDB = "Error al consultar la base de datos...";
              
            } else {
              msgDB = body2[0].mens;
              herramientas.escribirLOG("--> Respuesta DB:"  + JSON.stringify(body2));      
           } 



         

        herramientas.escribirLOG("--> Respuesta msg DB:"  + msgDB);



            /**Respuesta final */
            response = {
              "msgIA": respJson.output.text,
              "msgBD": msgDB
            }

            res.status(200);


            herramientas.escribirLOG("--> Respuesta del motor de IA:"  + " " + respuesta + " <END>");
            res.send(response);





      
      });
  
      }

    });
} //****** dialogar





module.exports = {
  dialogar
}
