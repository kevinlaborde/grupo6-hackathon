/**
 * @name App
 * @version 1.0.0
 * @namespace
 * @description Aplicación principal
 */
var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');
var requestJSON = require('request-json');

var globales = require('./globales');

var herramientas = require('./control/herram');
var dialogoControl = require('./control/dialogo');

const cors = require('cors')
var app = express();

app.use(cors());
var port = process.env.PORT || 3000;
const URI = '/v1/';

var baseMLabURL = globales.baseMLabURL;
var apikeyMLab = globales.apikeyMLab;


// app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json({
  limit: '10mb'
}));


app.listen(port);
console.log('Escuchando en el puerto:' + port + '/tcp');


/** @method
 * @public
 * @name appSaludar
 * @summary Envia saludo.
 * @description Este método nos sirve de prueba para saber que la API está on line. <br>
 * Invocación -> [GET]/v1/
 * @example localhost:3000/v1/
 * @author Grupo 6
 * @memberof App
 */
app.get(URI, herramientas.saludar);


/** @method @public
 * @name appDialogar
 * @summary Diálogo básico con el usuario
 * @description Controla una conversación mínima en un chat.<br>
 * Invocación -> [POST]/v1/dialogar
 * @example localhost:3000/v1/dialogar
 * @author Grupo 6
 * @memberof App
 */
app.post(URI + 'dialogar', dialogoControl.dialogar);

